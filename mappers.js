var _ = require('underscore');

function Field(options) {
    this.name = options.name;
    this.key = options.key;
    this.callback = options.callback;
};

Field.prototype.asValue = function(data) {
    if (this.callback) {
        var value = data[this.name];
        return this.callback(value, data);
    } else {
        return data[this.key];
    }
};


function Mapper(mapping) {
    var fields = _.map(mapping, function(mapping, name) {
        var field;
        if (_.isFunction(mapping)) {
            field = new Field({
                name: name,
                callback: mapping
            });
        } else {
            field = new Field({
                name: name,
                key: mapping
            });
        }
        return [name, field];
    });
    this.fields = _.object(fields);
}

Mapper.prototype.map = function(data) {
    var a = _.map(this.fields, function(field, key) {
        return [key, field.asValue(data)];
    });
    return _.object(a);
};


module.exports.create = function(mapping) {
    return function(obj) {
        var mapper = new Mapper(mapping);
        return mapper.map(obj);
    };
};

/*
module.exports.delegate = function(callback) {
    return new Field({ callback: callback });
};
 */

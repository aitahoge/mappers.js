var should = require('chai').should();
var mappers = require('../namaniku-mappers');


describe('create', function() {
    it('raw field', function() {
        var mapper = mappers.create({
            'id': 'id',
            'full_name': 'name'
        });
        
        var hirobumi = mapper({
            id: 1,
            name: 'Hirobumi Ito'
        });
        hirobumi.id.should.equal(1);
        hirobumi.full_name.should.equal('Hirobumi Ito');

        var kiyotaka = mapper({
            id: 2,
            name: 'Kiyotaka Kuroda'
        });
        kiyotaka.id.should.equal(2);
        kiyotaka.full_name.should.equal('Kiyotaka Kuroda');
    });

    it('function field', function() {
        var mapper = mappers.create({
            'id': 'id',
            'name': function(value, data) {
                return data.first_name + ' ' + data.last_name;
            }
        });
        
        var hirobumi = mapper({
            id: 1,
            first_name: 'Hirobumi',
            last_name: 'Ito'
        });
        hirobumi.id.should.equal(1);
        hirobumi.name.should.equal('Hirobumi Ito');
        hirobumi.should.not.have.property('first_name');
        hirobumi.should.not.have.property('last_name');

        var kiyotaka = mapper({
            id: 2,
            first_name: 'Kiyotaka',
            last_name: 'Kuroda'
        });
        kiyotaka.id.should.equal(2);
        kiyotaka.name.should.equal('Kiyotaka Kuroda');
        kiyotaka.should.not.have.property('first_name');
        kiyotaka.should.not.have.property('last_name');
    });

    it('nested mapper', function() {
        var ProfileMapper = mappers.create({
            age: 'age',
            sex: function(value) {
                switch (value) {
                    case 1:
                    return 'M';
                    case 2:
                    return 'F';
                }
                return null;
            }
        });

        var AccountMapper = mappers.create({
            id: 'id',
            first_name: function(value, data) {
                return data.name.split(' ')[0];
            },
            last_name: function(value, data) {
                var words = data.name.split(' ');
                return words[words.length-1];
            },
            profile: ProfileMapper
        });

        var tokibito = AccountMapper({
            id: 1,
            name: 'Shinya Okano',
            profile: {
                age: 29,
                sex: 1
            }
        });
        tokibito.id.should.equal(1);
        tokibito.first_name.should.equal('Shinya');
        tokibito.last_name.should.equal('Okano');
        tokibito.profile.age.should.equal(29);
        tokibito.profile.sex.should.equal('M');

        var cafistar = AccountMapper({
            id: 1,
            name: 'Mizuki Kuroda',
            profile: {
                age: 31,
                sex: 2
            }
        });
        cafistar.id.should.equal(1);
        cafistar.first_name.should.equal('Mizuki');
        cafistar.last_name.should.equal('Kuroda');
        cafistar.profile.age.should.equal(31);
        cafistar.profile.sex.should.equal('F');

        var monjudoh = AccountMapper({
            id: 1,
            name: 'Hideaki Nimura',
            profile: {
                sex: 0
            }
        });
        monjudoh.id.should.equal(1);
        monjudoh.first_name.should.equal('Hideaki');
        monjudoh.last_name.should.equal('Nimura');
        monjudoh.profile.should.not.have.property('age');
        (monjudoh.profile.sex === null).should.be.true;
    });
});
